<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@view');


Route::get('/query/1', 'QueryController@query1');

Route::get('/query/2', 'QueryController@query2');

Route::get('/query/3', 'QueryController@query3');

Route::get('/query/4', 'QueryController@query4');

Route::get('/query/5', 'QueryController@query5');

Route::get('/query/6', 'QueryController@query6');

Route::get('/query/7', 'QueryController@query7');

Route::get('/query/8', 'QueryController@query8');

Route::get('/query/10', 'QueryController@query10');