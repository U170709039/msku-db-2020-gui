@extends('layouts.app')

@section('content')

    <ul class="query-list">

        <h1>Mugla Sitki Kocman University</br>Powerlifting Database Project</h1>

        <h2>Queries</h2>

        <li><a target="_blank" href="{{url('query/1')}}">What are the average score values of each exercise type?</a></li>
        <li><a target="_blank" href="{{url('query/2')}}">What is the average value of the men's bench press score?</a></li>
        <li><a target="_blank" href="{{url('query/3')}}">What is the average value of the women's squat score?</a></li>
        <li><a target="_blank" href="{{url('query/4')}}">What is the number of the competitors according to their kg intervals?</a></li>
        <li><a target="_blank" href="{{url('query/5')}}">What is the maximum total score according to their kg intervals? </a></li>
        <li><a target="_blank" href="{{url('query/6')}}">Which age group has more participant? </a></li>
        <li><a target="_blank" href="{{url('query/7')}}">Which country has most events? </a></li>
        <li><a target="_blank" href="{{url('query/8')}}">How many participants are there by age classes?</a></li>
        <li><a target="_blank" href="{{url('query/10')}}">What is female and male participant rate?</a></li>

    </ul>


    <h2>Stats</h2>

    <table border="1">

        <thead>

            <tr>

                <th>Table</th>
                <th>Row Count</th>

            </tr>

        </thead>

        <tr>

            <td>Competitors</td>
            <td>{{$total['competitors']}}</td>

        </tr>

        <tr>

            <td>Age Classes</td>
            <td>{{$total['age_classes']}}</td>

        </tr>

        <tr>

            <td>Equipments</td>
            <td>{{$total['equipments']}}</td>

        </tr>

        <tr>

            <td>Formulas</td>
            <td>{{$total['formulas']}}</td>

        </tr>

        <tr>

            <td>Meets</td>
            <td>{{$total['meets']}}</td>

        </tr>

        <tr>

            <td>Scores</td>
            <td>{{$total['scores']}}</td>

        </tr>

    </table>


    <style>

        .query-list {text-align:left}

    </style>

@endsection