@extends('layouts.app')

@section('content')

    <h2>4. What is the number of the competitors according to their kg intervals?</h2>

    <table border="1">

		<?php foreach($data as $stat){ ?>
        <tr>

            <td><?php echo $stat->kg; ?></td>
            <td><?php echo $stat->count; ?></td>

        </tr>
		<?php } ?>

    </table>

@endsection