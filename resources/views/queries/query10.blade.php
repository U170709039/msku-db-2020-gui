@extends('layouts.app')

@section('content')

    <h2>What is female and male participant rate?</h2>

    <table border="1">

        @foreach($data as $row)
        <tr>

            <th>{{$row->sex}}</th>
            <td>{{$row->count}}</td>

        </tr>
        @endforeach

    </table>

@endsection