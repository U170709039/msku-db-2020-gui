@extends('layouts.app')

@section('content')

    <h2>6. Which age group has more participant?</h2>

    <table border="1">

        <thead>

        <tr>

            <th>Age Min</th>
            <th>Age Max</th>
            <th>Number of Competitors</th>

        </tr>

        </thead>

        @foreach($data as $row)
        <tr>

            <td>{{$row->age_min}}</td>
            <td>{{$row->age_max}}</td>
            <td>{{$row->count}}</td>

        </tr>
        @endforeach

    </table>

@endsection