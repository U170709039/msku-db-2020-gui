@extends('layouts.app')

@section('content')

    <h2>5. What is the maximum total score according to their kg intervals?</h2>

    <table border="1">

        <thead>

        <tr>

            <th>KG</th>
            <th>Max KG</th>

        </tr>

        </thead>

        @foreach($data as $row)
        <tr>

            <td>{{$row->kg}}</td>
            <td>{{$row->max_kg}}</td>

        </tr>
        @endforeach

    </table>

@endsection