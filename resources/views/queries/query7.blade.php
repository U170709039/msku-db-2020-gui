@extends('layouts.app')

@section('content')

    <h2>7. Which country has most events?</h2>

    <p><strong>{{$data->country}}:</strong> {{$data->count}}</p>

@endsection