@extends('layouts.app')

@section('content')

    <h2>1. What are the average score values of each exercise type?</h2>
    <table border="1">

        <thead>

        <tr>

            <td>KG</td>
            <td>Number of Competitors</td>

        </tr>

        </thead>

		<?php foreach($data as $value){ ?>
        <tr>
            <th><?php print_r($value->exercises_type); ?>:</th>
            <td><?php print_r($value->avg); ?></td>
        </tr>
		<?php } ?>

    </table>

@endsection