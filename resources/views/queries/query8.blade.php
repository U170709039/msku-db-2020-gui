@extends('layouts.app')

@section('content')

    <h2>How many participants are there by age classes?</h2>

    <table border="1">

        <thead>

            <tr>

                <th>Age Min</th>
                <th>Age Max</th>
                <th>Number of The Competitors</th>

            </tr>

        </thead>

        @foreach($data as $row)
        <tr>

            <td>{{$row->age_min}}</td>
            <td>{{$row->age_max}}</td>
            <td>{{$row->count}}</td>

        </tr>
        @endforeach

    </table>






@endsection