<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

	function view() {


		$stats = [

			'total' => [

				'competitors' => DB::table("competitors")->count(),
				'age_classes' => DB::table("age_classes")->count(),
				'equipments' => DB::table("equipments")->count(),
				'formulas' => DB::table("formulas")->count(),
				'meets' => DB::table("meets")->count(),
				'scores' => DB::table("scores")->count()

			]

		];


		return view('dashboard', $stats);

	}


}
