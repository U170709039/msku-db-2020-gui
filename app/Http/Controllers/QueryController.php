<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class QueryController extends Controller
{



	public function query1(){


		$query = DB::select(DB::raw("SELECT exercises_type, AVG(score_value) as avg 

FROM scores 

WHERE score_value>0 

GROUP BY exercises_type;"));


		return view('queries.query1', ['data'=>$query]);





	}



	public function query2(){


		$query = DB::select(DB::raw("SELECT avg(myMax) as avg

FROM (select max(score_value) AS myMax  

FROM scores   

LEFT JOIN competitors ON competitors.id = scores.competitors_id  

WHERE scores.exercises_type='bench'  AND scores.score_value>0 

GROUP BY scores.competitors_id) TEMP
"))[0]->avg;


		return view('queries.query2', ['data'=>$query]);


	}




	public function query3(){


		$query = DB::select(DB::raw("SELECT avg(myMax) as avg

FROM (SELECT max(score_value) AS myMax  

FROM competitors  

 LEFT JOIN scores ON competitors.id = scores.competitors_id 

WHERE competitors.sex='F' AND scores.exercises_type='squat' AND scores.score_value > 0 

GROUP BY scores.competitors_id) TMP"))[0]->avg;

		return view('queries.query3', ['data'=>$query]);


	}



	public function query4(){


		$query = DB::select(DB::raw("SELECT weight_class_kg as kg, COUNT(weight_class_kg) as count

FROM competitors 

GROUP BY weight_class_kg"));

		return view('queries.query4', ['data'=>$query]);


	}


	public function query5(){


		$query = DB::select(DB::raw("SELECT weight_class_kg as kg, MAX(total_kg) as max_kg

FROM competitors  

GROUP BY weight_class_kg"));

		return view('queries.query5', ['data'=>$query]);


	}



	public function query6(){


		$query = DB::select(DB::raw("SELECT age_classes.age_min, age_classes.age_max, COUNT(*) as count

FROM competitors 

LEFT JOIN age_classes ON age_classes.id = competitors.age_classes_id 

GROUP BY age_classes_id;"));

		return view('queries.query6', ['data'=>$query]);


	}


	public function query7(){


		$query = DB::select(DB::raw("SELECT country, COUNT(country) as count
FROM meets  
GROUP BY country  
HAVING COUNT(country)=(  
    SELECT MAX(FirstCountry)  
    FROM (  

        SELECT country, COUNT(id) AS FirstCountry FROM meets GROUP BY country  

    ) TMP  

);"));
		return view('queries.query7', ['data'=>$query[0]]);


	}




	public function query8(){


		$query = DB::select(DB::raw("SELECT age_classes.age_min, age_classes.age_max, COUNT(age_classes_id) as count

FROM competitors 

LEFT JOIN age_classes ON age_classes.id=competitors.age_classes_id 

GROUP BY age_classes_id
"));


		return view('queries.query8', ['data'=>$query]);


	}




	public function query10(){


		$query = DB::select(DB::raw("SELECT sex, COUNT(SEX) as count

FROM competitors 

GROUP BY SEX;"));


		return view('queries.query10', ['data'=>$query]);


	}


}
